# Aplicativo de Previsão do Tempo
Este é um aplicativo simples de previsão do tempo que utiliza a API do OpenWeatherMap para obter dados de localização e clima.

## Como usar
1. Acesse o aplicativo de previsão do tempo através do seguinte link: [Previsão do Tempo](https://clima-ashy-one.vercel.app/).
2. Permita o acesso à sua localização quando solicitado.
3. Digite o nome da cidade desejada no campo de busca e clique em "Buscar".
4. Os dados de temperatura atual, temperatura para as próximas 24 horas e para os próximos 5 dias serão exibidas.

## Recursos

Utiliza a API do OpenWeatherMap para obter dados de localização e clima em tempo real.
Usa HTML, CSS e JavaScript para a estrutura, estilo e funcionalidade do aplicativo.
Aplica conceitos como geolocalização e requisições assíncronas para melhorar a experiência do usuário.

## Contribuição
Contribuições são bem-vindas! Se você encontrar algum problema ou quiser melhorar o aplicativo de alguma forma, sinta-se à vontade para entrar em contato com os desenvolvedores do aplicativo.

## Autor
Allan Possani da Cruz (allanpcruz@gmail.com) - Desenvolvedor Principal

## Licença
Este projeto é licenciado sob a MIT License.
