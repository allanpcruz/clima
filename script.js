document.addEventListener('DOMContentLoaded', init);

document.querySelector('#buscar').addEventListener('click', buscarTempo);
const localidadeInput = document.querySelector('#localidade');
const apiKey = '6393825c10a114d8599998f569b64a57';

async function init() {
    try {
        const position = await getLocation();
        const { latitude, longitude } = position.coords;
        const locationUrl = `https://api.openweathermap.org/geo/1.0/reverse?lat=${latitude}&lon=${longitude}&limit=1&appid=${apiKey}`;

        const apiResponse = await fetch(locationUrl);

        if (apiResponse.ok) {
            const dadosLocalizacao = await apiResponse.json();
            const nomeCidade = dadosLocalizacao[0].name;
            localidadeInput.value = nomeCidade;
        } else {
            mostrarErro('Erro ao obter dados da localização.');
        }
    } catch (error) {
        mostrarErro('Erro ao obter dados da localização.');
    }
}

function getLocation() {
    return new Promise((resolve, reject) => {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(resolve, reject);
        } else {
            reject('Geolocalização não suportada pelo navegador.');
        }
    });
}

function mostrarErro(mensagem) {
    const mensagemErro = document.querySelector('#error-message');
    mensagemErro.textContent = mensagem;
}

async function buscarCoordenadas(cidade) {
    const coordsUrl = `https://api.openweathermap.org/geo/1.0/direct?q=${cidade}&appid=${apiKey}`;
    const coordsResponse = await fetch(coordsUrl);

    if (!coordsResponse.ok) {
        throw new Error('Erro ao obter informações de coordenadas.');
    }

    const dadosCoord = await coordsResponse.json();
    var loc = `${dadosCoord[0].name} - ${dadosCoord[0].state} - ${dadosCoord[0].country}`;
    const { lat, lon } = dadosCoord[0];
    return { lat, lon, loc };
}

async function buscarTempo() {
    mostrarErro('');
    const cidade = localidadeInput.value.trim();
    if (cidade === '') {
        mostrarErro('Informe uma cidade.');
        localidadeInput.focus();
        return;
    }

    try {
        const { lat, lon, loc } = await buscarCoordenadas(cidade);

        const climaUrl = `https://api.openweathermap.org/data/3.0/onecall?lat=${lat}&lon=${lon}&exclude=minutely&units=metric&appid=${apiKey}`;
        const climaResponse = await fetch(climaUrl);

        if (!climaResponse.ok) {
            throw new Error('Erro ao obter informações de clima.');
        }

        const dadosClima = await climaResponse.json();
        exibirDadosClima(dadosClima.current, loc);
        exibirProximasHoras(dadosClima.hourly);
        exibirProximosDias(dadosClima.daily);
    } catch (error) {
        console.log('🚀 ~ buscarTempo ~ error:', error);
        mostrarErro(`Localidade não encontrada`);
    }
}

function exibirDadosClima(dados, localidade) {
    const timestamp = dados.dt;
    const data = new Date(timestamp * 1000);
    const dia = data.getDate();
    const mes = data.getMonth() + 1;
    const ano = data.getFullYear();
    const hora = data.getHours();
    const minuto = data.getMinutes();

    document.querySelector('#temperatura').textContent = `${dados.temp.toFixed(0)}°C`;
    document.querySelector('#cidade').textContent = localidade;
    document.querySelector('#timestamp-consulta').textContent = `${fixZero(dia)}/${fixZero(mes)}/${fixZero(ano)} - ${fixZero(hora)}:${fixZero(minuto)}`;
}

function exibirProximosDias(proximosDias) {
    const proximasDatas = proximosDias.slice(1, 6);
    const dadosPrevisao = proximasDatas.map(day => {
        return {
            date: new Date(day.dt * 1000).toLocaleDateString('pt-BR', { day: '2-digit', month: '2-digit' }),
            temp_min: day.temp.min.toFixed(0),
            temp_max: day.temp.max.toFixed(0),
        };
    });
    let dadosExibir = '';
    dadosPrevisao.forEach(day => {
        dadosExibir += `
            <div class="bg-gray-200 p-2 rounded-lg flex flex-col items-center">
                <p class="text-lg font-semibold mb-1">${day.date}</p>
                <p class="text-gray-700"><i class="fas fa-thermometer-full"></i> ${day.temp_max}°C</p>
                <p class="text-gray-700"><i class="fas fa-thermometer-empty"></i> ${day.temp_min}°C</p>
            </div>
        `;
    });

    document.querySelector('#proximos-dias-container').innerHTML = dadosExibir;
}

function exibirProximasHoras(proximasHoras) {
    let dadosExibir = '<div class="flex space-x-4">';
    proximasHoras.map(hour => {
        const data = new Date(hour.dt * 1000);
        dadosExibir += `
            <div class="bg-gray-200 rounded-lg flex flex-col items-center p-1">
                <p class="text-xs font-semibold mb-1">${fixZero(data.getHours())}:${fixZero(data.getMinutes())}</p>
                <p class="text-xs text-gray-700 mb-2">${hour.temp.toFixed(0)}°C</p>
            </div>
        `;
    });

    dadosExibir += '</div>';

    document.querySelector('#proximas-horas-container').innerHTML = dadosExibir;
}

function fixZero(time) {
    return time < 10 ? '0' + time : time;
}
